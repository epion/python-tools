from src.back_end.input_parser import parse_input_args
from src.back_end.simulation import plot_efficiency, run_simulation


def main(args):
    if args.mode in ('simulation', 'all'):
        run_simulation(args.robots_count, args.speed, args.width, args.height, args.coverage, args.trials,
                       args.robot_type)
    if args.mode in ('plot', 'all'):
        plot_efficiency('Comparison of StandardRobot and RandomWalkRobot', 'number of robots',
                        'iterations to clean', args)


if __name__ == '__main__':
    main(parse_input_args())
