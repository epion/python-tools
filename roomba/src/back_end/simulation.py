import pylab

from src.back_end.robots import RandomWalkRobot, StandardRobot
from src.back_end.rooms import RectangularRoom
from src.front_end.display import RobotVisualization


def run_simulation(num_robots, speed, width, height, min_coverage, num_trials, robot_type, plot=False):
    """
    Runs NUM_TRIALS trials of the simulation and returns the mean number of
    time-steps needed to clean the fraction MIN_COVERAGE of the room.
    The simulation is run with NUM_ROBOTS robots of type ROBOT_TYPE, each with
    speed SPEED, in a room of dimensions WIDTH x HEIGHT.

    num_robots: an int (num_robots > 0)
    speed: a float (speed > 0)
    width: an int (width > 0)
    height: an int (height > 0)
    min_coverage: a float (0 <= min_coverage <= 1.0)
    num_trials: an int (num_trials > 0)
    robot_type: class of robot to be instantiated (e.g. StandardRobot)
    """
    iterations_needed = []
    for sim in range(num_trials):
        anim = RobotVisualization(num_robots, width, height, plot, delay=0.2)
        room = RectangularRoom(width, height)
        robots = [robot_type(room, speed) for _ in range(num_robots)]
        counter, coverage = 0, 0
        while coverage < min_coverage:
            counter += 1
            anim.update(room, robots)
            [robot.update_position_and_clean() for robot in robots]
            coverage = room.get_clean_tile_count() / room.get_tile_count()
        anim.update(room, robots)
        iterations_needed.append(counter)
        anim.finished()
    return sum(iterations_needed) / max(len(iterations_needed), 1)


def plot_efficiency(title, x_label, y_label, args):
    robot_range = range(1, args.robots_count + 1)
    times1, times2 = [], []
    for num_robots in robot_range:
        print("Plotting", num_robots, "robots...")
        times1.append(run_simulation(num_robots, args.speed, args.width, args.height, args.coverage, args.trials,
                                     StandardRobot, plot=True))
        times2.append(run_simulation(num_robots, args.speed, args.width, args.height, args.coverage, args.trials,
                                     RandomWalkRobot, plot=True))
    pylab.plot(robot_range, times1)
    pylab.plot(robot_range, times2)
    pylab.title(title)
    pylab.legend(('StandardRobot', 'RandomWalkRobot'))
    pylab.xlabel(x_label)
    pylab.ylabel(y_label)
    pylab.show()
