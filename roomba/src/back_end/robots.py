import random

from src.back_end.rooms import Position


class MetaClass(type):
    """Used to display human-readable string in argparse output."""
    def __str__(self):
        return self.__name__


class Robot:
    def __init__(self, room, speed):
        self.room = room
        self.speed = speed
        self.x = random.choice([_x for _x in range(room.width)])
        self.y = random.choice([_y for _y in range(room.height)])
        self.direction = random.choice([angle for angle in range(360)])
        self.position = Position(self.x, self.y)

    def set_robot_position(self, position):
        self.position = position
        self.x = position.x
        self.y = position.y

    def update_position_and_clean(self):
        """
        Simulate the passage of a single time-step.
        Move the robot to a new position and mark the tile it is on as having been cleaned.
        """
        raise NotImplementedError


class StandardRobot(Robot, metaclass=MetaClass):
    """
    At each time-step, a StandardRobot attempts to move in its current
    direction; when it would hit a wall, it *instead* chooses a new direction
    randomly.
    """
    def __init__(self, room, speed):
        super().__init__(room, speed)
        super().__repr__()
        self.room.clean_tile(self.position)

    def update_position_and_clean(self):
        new_pos = self.position.get_new_position(self.direction, self.speed)
        if 0 <= new_pos.x < self.room.width and 0 <= new_pos.y < self.room.height:
            self.position = new_pos
            self.room.clean_tile(self.position)
        else:
            self.direction = random.choice([angle for angle in range(360)])


class RandomWalkRobot(Robot, metaclass=MetaClass):
    """
    A RandomWalkRobot is a robot with the "random walk" movement strategy: it
    chooses a new direction at random at the end of each time-step.
    """
    def __init__(self, room, speed):
        super().__init__(room, speed)
        self.room.clean_tile(self.position)

    def update_position_and_clean(self):
        new_pos = self.position.get_new_position(self.direction, self.speed)
        if 0 <= new_pos.x < self.room.width and 0 <= new_pos.y < self.room.height:
            self.position = new_pos
            self.room.clean_tile(self.position)
        self.direction = random.choice([angle for angle in range(360)])
