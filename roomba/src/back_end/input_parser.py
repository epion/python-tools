import argparse

from src.back_end.robots import RandomWalkRobot, StandardRobot


def parse_mode(mode):
    if mode.lower() in ['simulation', 'sim', 'simul', 'visual', 'graphic']:
        return 'simulation'
    elif mode.lower() in ['plot', 'stats', 'stat', 'statistics', 'data', 'comp', 'comparison']:
        return 'plot'
    elif mode.lower() in ['all', 'full', 'total', 'complete']:
        return 'all'


def parse_robot_type(rtype):
    if rtype.lower() in ['std', 's', 'standard', 'default', 'normal', 'standardrobot', 'standard_robot']:
        return StandardRobot
    elif rtype.lower() in ['random', 'other', 'randomwalk', 'randomwalkrobot', 'random_walk', 'random_walk_robot']:
        return RandomWalkRobot


def parse_input_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--mode', choices=['simulation', 'plot', 'all'], type=parse_mode, required=True)
    parser.add_argument('-rc', '--robots_count', default=2, type=int)
    parser.add_argument('-s', '--speed', default=1.0, type=float)
    parser.add_argument('-we', '--width', default=8, type=int)
    parser.add_argument('-he', '--height', default=8, type=int)
    parser.add_argument('-c', '--coverage', default=0.9, type=float)
    parser.add_argument('-t', '--trials', default=1, type=int)
    parser.add_argument('-rt', '--robot_type', default='standard', choices=[StandardRobot, RandomWalkRobot],
                        type=parse_robot_type)
    return parser.parse_args()
