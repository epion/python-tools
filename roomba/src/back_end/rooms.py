import math
import random


class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f'({self.x:.2f}, {self.y:.2f})'

    def __repr__(self):
        return f'Object representing position. ({self.x:.2f}, {self.y:.2f})'

    def get_new_position(self, angle, speed):
        """
        Computes and returns the new Position after a single clock-tick has
        passed, with this object as the current position, and with the
        specified angle and speed.
        angle: number representing angle in degrees, 0 <= angle < 360
        speed: positive float representing speed
        Returns: a Position object representing the new position.
        """
        delta_x, delta_y = speed * math.sin(math.radians(angle)), speed * math.cos(math.radians(angle))
        new_x, new_y = self.x + delta_x, self.y + delta_y
        return Position(new_x, new_y)


class RectangularRoom:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.tiles = {(x, y): 0 for x in range(self.width) for y in range(self.height)}
        self.positions = {Position(x, y): 0 for x in range(self.width) for y in range(self.height)}

    def clean_tile(self, pos):
        """Assumes that POS represents a valid position inside this room."""
        self.tiles[(int(pos.x), int(pos.y))] = 1
        self.positions[pos] = 1

    def is_tile_cleaned(self, x, y):
        """Assumes that (x, y) represents a valid tile inside the room."""
        cleaned = self.tiles.get((x, y), False)
        return True if cleaned else False

    def get_tile_count(self):
        return self.width * self.height

    def get_clean_tile_count(self):
        return sum([1 for (x, y), cleaned in self.tiles.items() if cleaned])

    def get_random_position(self):
        return random.choice(list(self.positions.keys()))

    def is_position_in_room(self, pos):
        return True if ((0 <= pos.x < self.width) and (0 <= pos.y < self.height)) else False
