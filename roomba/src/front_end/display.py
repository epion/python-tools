import math
import time

from tkinter import *


class RobotVisualization:
    __slots__ = ['delay', 'max_dim', 'width', 'height', 'num_robots', 'tiles', 'master', 'canvas', 'robots', 'text',
                 'time', 'plot']

    def __init__(self, num_robots, width, height, plot, delay=0.2):
        self.delay = delay
        self.max_dim = max(width, height)
        self.width = width
        self.height = height
        self.num_robots = num_robots
        self.tiles = {}
        self.master = self.canvas = self.robots = self.text = self.time = None
        self.time = 0
        self.plot = plot
        if not self.plot:
            self._draw_surface()
            self._draw_background()
            self._draw_grid()
            self._draw_dirty_tiles()
            self._draw_status_text(0)
            self.master.update()

    def _draw_surface(self):
        self.master = Tk()
        self.canvas = Canvas(self.master, width=500, height=500)
        self.canvas.pack()
        self.master.update()

    def _draw_background(self):
        x1, y1 = self._map_coords(0, 0)
        x2, y2 = self._map_coords(self.width, self.height)
        self.canvas.create_rectangle(x1, y1, x2, y2, fill="white")

    def _draw_grid(self):
        for i in range(self.width + 1):
            x1, y1 = self._map_coords(i, 0)
            x2, y2 = self._map_coords(i, self.height)
            self.canvas.create_line(x1, y1, x2, y2)
        for i in range(self.height + 1):
            x1, y1 = self._map_coords(0, i)
            x2, y2 = self._map_coords(self.width, i)
            self.canvas.create_line(x1, y1, x2, y2)

    def _draw_dirty_tiles(self):
        for i in range(self.width):
            for j in range(self.height):
                x1, y1 = self._map_coords(i, j)
                x2, y2 = self._map_coords(i + 1, j + 1)
                self.tiles[(i, j)] = self.canvas.create_rectangle(x1, y1, x2, y2, fill="gray")

    def _draw_clean_tiles(self, room):
        for i in range(self.width):
            for j in range(self.height):
                if room.is_tile_cleaned(i, j):
                    self.canvas.delete(self.tiles[(i, j)])

    def _draw_status_text(self, clean_tile_count):
        self.text = self.canvas.create_text(25, 0, anchor=NW, text=self._status_text(self.time, clean_tile_count))

    def _status_text(self, time, num_clean_tiles):
        percent_clean = round(100 * num_clean_tiles / (self.width * self.height))
        return f'Iteration: {time}; {num_clean_tiles} tiles ({percent_clean}% cleaned)'

    def _update_text(self, room):
        self.canvas.delete(self.text)
        self.time += 1
        self._draw_status_text(room.get_clean_tile_count())

    def _map_coords(self, x, y):
        """Maps grid positions to window positions (in pixels)."""
        return (250 + 450 * ((x - self.width / 2.0) / self.max_dim),
                250 + 450 * ((self.height / 2.0 - y) / self.max_dim))

    def _draw_robot(self, pos, direction):
        d1, d2 = direction + 165, direction - 165
        x1, y1 = self._map_coords(pos.x, pos.y)
        x2, y2 = self._map_coords(pos.x + 0.6 * math.sin(math.radians(d1)),
                                  pos.y + 0.6 * math.cos(math.radians(d1)))
        x3, y3 = self._map_coords(pos.x + 0.6 * math.sin(math.radians(d2)),
                                  pos.y + 0.6 * math.cos(math.radians(d2)))
        return self.canvas.create_polygon([x1, y1, x2, y2, x3, y3], fill="red")

    def _delete_old_robots(self):
        if self.robots:
            for robot in self.robots:
                self.canvas.delete(robot)
                self.master.update_idletasks()
        self.robots = []

    def _draw_new_robots(self, robots):
        for robot in robots:
            pos = robot.position
            x1, y1 = self._map_coords(pos.x - 0.08, pos.y - 0.08)
            x2, y2 = self._map_coords(pos.x + 0.08, pos.y + 0.08)
            self.robots.append(self.canvas.create_oval(x1, y1, x2, y2, fill="black"))
            self.robots.append(self._draw_robot(robot.position, robot.direction))

    def update(self, room, robots):
        if not self.plot:
            self._draw_clean_tiles(room)
            self._delete_old_robots()
            self._draw_new_robots(robots)
            self._update_text(room)
            self.master.update()
            time.sleep(self.delay)

    def finished(self):
        if not self.plot:
            mainloop()
