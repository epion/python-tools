# Python tools

## Roomba

**Roomba**, mostly based on an MIT course exercise, is a tool to simulate a vacuum cleaner robot, cleaning a room. 

(That is pretty much all it does, really.)

You can run it in two specific modes:
1. Graphical simulation that shows you how long it will take a robot/-s of specific kind and parameters
to clean a room
2. Plot displaying a comparison of cleaning speed between different kind of robots.

### How do I run that?
    
    parser.add_argument('-m', '--mode', choices=['simulation', 'plot', 'all'], type=parse_mode, required=True)
    parser.add_argument('-rc', '--robots_count', default=2, type=int)
    parser.add_argument('-s', '--speed', default=1.0, type=float)
    parser.add_argument('-we', '--width', default=8, type=int)
    parser.add_argument('-he', '--height', default=8, type=int)
    parser.add_argument('-c', '--coverage', default=0.9, type=float)
    parser.add_argument('-t', '--trials', default=1, type=int)
    parser.add_argument('-rt', '--robot_type', default='standard', choices=[StandardRobot, RandomWalkRobot],
                        type=parse_robot_type)



## JTMS
**JTMS**, aka *Jira Test Management System* is a highly efficient and lightweight python tool that transforms your local JIRA instance
into a professional testcase management system!

The methods used in this tool are designed to automatize all JIRA-related testcase management work, such as:
* Uploading testcases to JIRA
* Updating testcases on JIRA 
  * Testcase specification, if changed between testrounds
  * Testcase with test results from latest testround
 * Assigning proper components
 * Assigning people responsible for fixing the feature represented by the testcase
 * Performing transitions depending on test results - based on JSM (Jira StateMachine)
 
All of this is done asynchronically with the usage of aiohttp library - this is because
pure JIRA REST api is much more efficient than communication with the usage of available
JIRA python libraries. Those libraries do not support asynchronous programming, only
concurrent if anything.

Such an update is actually being done FASTER than the update you would perform using 
built-in JIRA bulk change mechanisms. **Fantastic, isn't it?!**

The only bottleneck is your server's hardware -  precisely your CPU, which frankly
performs a load of work when sent many asynchronous requests from a single IP. Your
aiohttp ClientSession suggested connection pool should be set to something 40-ish to avoid
killing your server's CPU.

However, mind that by default JIRA has a 20 connections connection pool, so you might
need to tweak that to avoid excessive connection queueing which could lead to shutting
down your client session.
 
### But how do I actually use this stuff?

The usage of *this stuff* is made simple, as one would guess!
 
 
But don't take my word for it, rather take a look at the step-by-step instruction:
1. Edit your **config/jtms.ini** file and provide appropriate values for your usage.
2. You can optionally provide a path to a single test results archive
    * -tr/--test_results {path_to_results}

You do not need to provide the above-mentioned argument, though - this stuff is automatic,
remember?

If there is no input argument provided, **path_results** is taken from your config file.
The script will scan through the directory provided and grab the newest
testround results archive files. 

### How about an example?

